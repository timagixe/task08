const express = require('express');
const path = require('path');
const fs = require('fs');

const app = express();
const cors = require('cors');

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'client/public')));

app.get('/messages', (req, res) => {
    const messages = require('./messages.json');

    if (messages) {
        res.send(messages);
    } else {
        res.send(404);
    }
});

app.post('/login', (req, res) => {
    const users = require('./users.json');
    const { email, password } = req.body;
    let found = false;
    let userC;
    users.forEach((user) => {
        if (user.email === email && user.password === password) {
            found = true;
            userC = user;
        }
    });
    if (found) {
        res.send(userC).end();
    } else {
        throw new Error('User not found');
    }
});

app.get('/get-users/', (req, res) => {
    const users = require('./users.json');

    res.send(users);
});

app.get('/users/:id', (req, res) => {
    const userId = req.params.id;
    const users = require('./users.json');
    let found = false;
    users.forEach((user) => {
        if (user.userId === userId) {
            found = true;
            res.send(user);
        }
    });
    if (!found) {
        res.status(404).send('no such user');
    }
});

app.post('/users/:id', (req, res) => {
    const userId = req.params.id;
    const newUserData = req.body;
    const users = require('./users.json');

    if (userId) {
        const newUsers = users.map((user) => {
            if (user.userId === userId) {
                user = newUserData;
            }
            return user;
        });

        fs.writeFileSync('./users.json', JSON.stringify(newUsers), (err) => {
            if (err) {
                res.status(500).send('failed to save data');
            }
        });

        res.send(newUserData);
    }
});

app.post('/users/', (req, res) => {
    const newUser = req.body;
    newUser.userId = String(Math.floor(Math.random() * 100000));
    const users = require('./users.json');
    const newUsers = users.concat(newUser);
    fs.writeFileSync('./users.json', JSON.stringify(newUsers), (err) => {
        if (err) {
            res.sendStatus(500).send('failed to save data');
        }
    });
    res.send(newUser);
});

app.delete('/users/:id', (req, res) => {
    const userIdToDelete = req.params.id;
    const users = require('./users.json');
    const newUsers = users.filter((user) => user.userId !== userIdToDelete);

    fs.writeFileSync('./users.json', JSON.stringify(newUsers), (err) => {
        if (err) {
            res.sendStatus(500).send('failed to save data');
        }
    });
    res.send(`User with id ${userIdToDelete} was deleted`);
});

const port = process.env.PORT || 5000;
app.listen(port);

console.log('App is listening on port http://localhost:' + port);
