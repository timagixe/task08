import React from 'react';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Switch,
} from 'react-router-dom';
import createSagaMiddleware from 'redux-saga';

import Chat from './containers/Chat/Chat';
import Login from './components/Login/Login';
import Users from './containers/Users/Users';
import mainReducer from './store/reducers/mainReducer';
import modalReducer from './store/reducers/modalReducer';
import inputReducer from './store/reducers/inputReducer';
import usersReducer from './store/reducers/usersReducer';
import userPageReducer from './store/reducers/userPageReducer';
import loginReducer from './store/reducers/loginReducer';
import messagesReducer from './store/reducers/messagesReducer';

import './App.css';
import UserEditor from './components/UserEditor/UserEditor';
import { allSagas } from './store/sagas/sagas';

const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
    mainReducer: mainReducer,
    modalReducer: modalReducer,
    inputReducer: inputReducer,
    usersReducer: usersReducer,
    userPageReducer: userPageReducer,
    loginReducer: loginReducer,
    messagesReducer: messagesReducer,
});

const store = createStore(
    rootReducer,
    compose(
        applyMiddleware(sagaMiddleware),
        window.__REDUX_DEVTOOLS_EXTENSION__
            ? window.__REDUX_DEVTOOLS_EXTENSION__()
            : (f) => f
    )
);

sagaMiddleware.run(allSagas);

function App() {
    return (
        <Provider store={store}>
            <Router>
                <Switch>
                    <Route exact path='/login' component={Login} />
                    <Route exact path='/users' component={Users} />
                    <Route path='/edit/:id' component={UserEditor} />
                    <Route path='/edit/' component={UserEditor} />
                    <Route exact path='/chat' component={Chat} />
                    <Redirect to='/login' />
                </Switch>
            </Router>
        </Provider>
    );
}

export default App;
