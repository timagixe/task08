import React from 'react';
import { connect } from 'react-redux';
import User from '../../components/User/User';
import * as actions from '../../store/actions/actionCreators';

class Users extends React.Component {
    addUserHandler() {
        this.props.resetProps();
        this.props.history.push('/edit/');
    }

    componentDidMount() {
        this.props.onFetchUsersList();

        if (
            !(
                !!sessionStorage.getItem('email') &&
                sessionStorage.getItem('email') === 'admin'
            )
        ) {
            this.props.history.push('/chat');
        }
    }

    render() {
        const usersList = this.props.users.map((user) => (
            <User key={user.userId} history={this.props.history} {...user} />
        ));

        return (
            <>
                <button
                    onClick={() => {
                        this.addUserHandler();
                    }}
                >
                    Add User
                </button>
                <button
                    onClick={() => {
                        this.props.history.push('/chat');
                    }}
                >
                    Open Chat
                </button>
                {usersList}
            </>
        );
    }
}

const mapStateToProps = (state) => ({
    users: state.usersReducer.users,
});

const mapDispatchToProps = (dispatch) => ({
    onFetchUsersList: () => dispatch(actions.fetchUsersList()),
    onDeleteUser: (id) => dispatch(actions.deleteUser(id)),
    resetProps: () => dispatch(actions.resetProps()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Users);
