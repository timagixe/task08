import React from 'react';
import Spinner from '../../components/Spinner/Spinner';
import Header from '../../components/Header/Header';
import Messages from '../../components/Messages/Messages';
import MessageInput from '../MessageInput/MessageInput';
import Modal from '../Modal/Modal';
import * as actions from '../../store/actions/actionCreators';
import { connect } from 'react-redux';

import './Chat.css';

class Chat extends React.Component {
    componentDidMount() {
        this.props.onFetchMessages();
        if (sessionStorage.getItem('email')) {
            setTimeout(this.fetchMessagesHandler.bind(this), 1000);
        } else {
            this.props.history.push('/login');
        }
    }

    async fetchMessagesHandler() {
        const response = await fetch(
            'https://edikdolynskyi.github.io/react_sources/messages.json'
        );

        let responseArray = await response.json();

        this.props.onUpdateMessagesWithFetched(responseArray);
        this.props.onLoading();
    }

    getChatParticipantsCountHandler = () => {
        let arrayOfUniqueUsers = [];

        this.props.messages.map(({ user }) => {
            if (!arrayOfUniqueUsers.includes(user)) {
                arrayOfUniqueUsers = arrayOfUniqueUsers.concat(user);
            }
            return arrayOfUniqueUsers;
        });

        return arrayOfUniqueUsers.length;
    };

    getChatMessagesCountHandler = () => this.props.messages.length;

    getLastMessageTimeHandler = () => {
        let messages = this.props.messages;

        messages = messages.sort(function (a, b) {
            return new Date(b.createdAt) - new Date(a.createdAt);
        });

        let lastMessageWasSentAt = new Date(messages[0].createdAt)
            .toString()
            .split(' ')[4]
            .split(':');

        return `${lastMessageWasSentAt[0]}:${lastMessageWasSentAt[1]}`;
    };

    render() {
        return (
            <div className='chat_window'>
                {this.props.loading ? (
                    <Spinner />
                ) : (
                    <React.Fragment>
                        <Modal show={this.props.showModal} />
                        <Header
                            chatParticipantsCount={this.getChatParticipantsCountHandler()}
                            chatMessagesCount={this.getChatMessagesCountHandler()}
                            lastMessageTime={this.getLastMessageTimeHandler()}
                        />
                        <Messages messages={this.props.messages.reverse()} />
                        <MessageInput />
                    </React.Fragment>
                )}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    loading: state.mainReducer.loading,
    messages: state.mainReducer.messages,
    showModal: state.modalReducer.showModal,
    messagesAPI: state.messagesReducer,
});

const mapDispatchToProps = (dispatch) => ({
    onLoading: () => dispatch(actions.stopLoading()),
    onUpdateMessagesWithFetched: (messagesArray) =>
        dispatch(actions.updateMessagesWithFetched(messagesArray)),
    onFetchMessages: () => dispatch(actions.fetchMessages()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
