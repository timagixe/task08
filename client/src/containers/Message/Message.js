import React from 'react';
import { faHeart, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as actions from '../../store/actions/actionCreators';

import './Message.css';
import { connect } from 'react-redux';

class Message extends React.Component {
    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    scrollToBottom() {
        this.el.scrollIntoView({ behavior: 'smooth' });
    }

    likeMessageHandler(id) {
        const heartStyle = document.getElementById(id).className;
        if (heartStyle === 'liked') {
            document.getElementById(id).className = 'not-liked';
        } else {
            document.getElementById(id).className = 'liked';
        }
    }

    render() {
        const { avatarUrl, text, userName } = this.props;

        let createdAt = new Date(this.props.createdAt)
            .toString()
            .split(' ')[4]
            .split(':');

        createdAt = `${createdAt[0]}:${createdAt[1]}`;

        const selectOneOfTwo = (ifTrueValue, ifFalseValue) =>
            userName === 'Yourself' ? ifTrueValue : ifFalseValue;

        const removeOrEditIconsElement = (
            <React.Fragment>
                <div
                    className='remove'
                    onClick={() => {
                        this.props.onDeleteMessage(this.props.id);
                    }}
                >
                    <FontAwesomeIcon icon={faTrash} />
                </div>
                <div
                    className='edit'
                    onClick={() => {
                        this.props.onShowModal(this.props.text, this.props.id);
                        // const text = prompt('ENTER NEW TEXT');

                        // if (text !== null) {
                        //     text.trim() === ''
                        //         ? alert('MESSAGE TEXT CANNOT BE EMPTY')
                        //         : this.props.onEditMessage(this.props.id, text);
                        // }
                    }}
                >
                    <FontAwesomeIcon icon={faEdit} />
                </div>
            </React.Fragment>
        );

        const likeIconElement = (
            <div
                className='not-liked'
                id={`${this.props.id}_like`}
                onClick={() => {
                    this.likeMessageHandler(`${this.props.id}_like`);
                }}
            >
                <FontAwesomeIcon icon={faHeart} />
            </div>
        );

        return (
            <li
                ref={(el) => {
                    this.el = el;
                }}
                className={selectOneOfTwo(
                    'message right appeared',
                    'message left appeared'
                )}
                id={this.props.id}
            >
                {userName === 'Yourself' ? null : (
                    <img
                        className='avatar'
                        src={avatarUrl}
                        alt={`${userName}'s avatar`}
                        title={`${userName}`}
                    ></img>
                )}

                <div className='text_wrapper'>
                    {selectOneOfTwo(removeOrEditIconsElement, likeIconElement)}

                    <div
                        className={
                            this.props.editedAt
                                ? selectOneOfTwo(
                                      'date-yourself edited',
                                      'date edited'
                                  )
                                : selectOneOfTwo('date-yourself', 'date')
                        }
                        title={
                            this.props.editedAt
                                ? `Edited at ${new Date(
                                      this.props.editedAt
                                  ).toString()}`
                                : ''
                        }
                    >
                        {createdAt}
                    </div>
                    <div className='text'>{text}</div>
                </div>
            </li>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    onDeleteMessage: (messageId) => dispatch(actions.deleteMessage(messageId)),
    onEditMessage: (messageId, messageText) =>
        dispatch(actions.editMessage(messageId, messageText)),
    onShowModal: (currentMessageText, currentMessageId) =>
        dispatch(
            actions.changeModalStatus(currentMessageText, currentMessageId)
        ),
});

export default connect(null, mapDispatchToProps)(Message);
