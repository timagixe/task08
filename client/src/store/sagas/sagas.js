import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import * as actions from '../actions/actionCreators';
import * as actionTypes from '../actions/actionTypes';

const API = 'http://localhost:5000';

let axiosConfig = {
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': '*',
    },
};

export function* fetchUser(action) {
    try {
        const response = yield call(axios.get, `${API}/users/${action.userId}`);
        yield put(actions.fetchUserSuccess(response));
    } catch (error) {
        yield put(actions.fetchUserFailed(error));
    }
}

function* watchFetchUser() {
    yield takeEvery(actionTypes.FETCH_USER, fetchUser);
}

export function* saveUserEditorData(action) {
    try {
        if (action.id) {
            const response = yield call(
                axios.post,
                `${API}/users/${action.userData.userId}`,
                action.userData,
                axiosConfig
            );

            yield put(actions.saveUserEditorDataSuccess(response.data));
        } else {
            const response = yield call(
                axios.post,
                `${API}/users/`,
                action.userData,
                axiosConfig
            );
            yield put(actions.saveUserEditorDataSuccess(response.data));
        }
    } catch (error) {
        yield put(actions.saveUserEditorDataFailed(error));
    }
}

function* watchSaveUserEditorData() {
    yield takeEvery(actionTypes.SAVE_USER_EDITOR_DATA, saveUserEditorData);
}

export function* fetchUsersListSaga(action) {
    try {
        const response = yield call(axios.get, `${API}/get-users/`);
        yield put(actions.fetchUsersListSuccess(response.data));
    } catch (error) {
        yield put(actions.fetchUsersListFailed(error));
    }
}

function* watchFetchUsersList() {
    yield takeEvery(actionTypes.FETCH_USERS_LIST, fetchUsersListSaga);
}

export function* deleteUser(action) {
    try {
        const response = yield call(
            axios.delete,
            `${API}/users/${action.userId}`
        );

        yield put(actions.deleteUserSuccess(response.data));
    } catch (error) {
        yield put(actions.deleteUserFailed(error));
    }
}

function* watchDeleteUser() {
    yield takeEvery(actionTypes.DELETE_USER, deleteUser);
}

export function* loginUser(action) {
    try {
        const response = yield call(axios.post, `${API}/login/`, {
            email: action.email,
            password: action.password,
        });

        yield put(actions.loginUserSuccess(response.data));
    } catch (error) {
        yield put(actions.loginUserFailed(error.message));
    }
}

function* watchLoginUser() {
    yield takeEvery(actionTypes.LOGIN_USER, loginUser);
}

export function* fetchMessages(action) {
    try {
        const response = yield call(axios.get, `${API}/messages/`);
        yield put(actions.fetchMessagesSuccess(response.data));
    } catch (error) {
        yield put(actions.fetchMessagesFailed(error));
    }
}

function* watchFetchMessages() {
    yield takeEvery(actionTypes.FETCH_MESSAGES, fetchMessages);
}

export function* allSagas() {
    yield all([
        watchFetchMessages(),
        watchLoginUser(),
        watchDeleteUser(),
        watchFetchUsersList(),
        watchFetchUser(),
        watchSaveUserEditorData(),
    ]);
}
