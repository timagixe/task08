import * as actionTypes from './actionTypes';

export const stopLoading = () => ({
    type: actionTypes.STOP_LOADING,
});

export const updateMessagesWithFetched = (messagesArray) => ({
    type: actionTypes.UPDATE_MESSAGES_WITH_FETCHED,
    messages: messagesArray,
});

export const submitMessage = (messageObject) => ({
    type: actionTypes.SUBMIT_MESSAGE,
    message: messageObject,
});

export const deleteMessage = (messageId) => ({
    type: actionTypes.DELETE_MESSAGE,
    messageId: messageId,
});

export const editMessage = (messageId, messageText) => ({
    type: actionTypes.EDIT_MESSAGE,
    messageId: messageId,
    messageText: messageText,
});

export const changeModalStatus = (messageText, messageId) => ({
    type: actionTypes.CHANGE_MODAL_STATUS,
    messageText,
    messageId,
});

export const changeModalText = (newText) => ({
    type: actionTypes.CHANGE_MODAL_TEXT,
    newText,
});

export const submitModalText = (messageText, messageId) => ({
    type: actionTypes.SUBMIT_MODAL_TEXT,
    messageText,
    messageId,
});

export const inputChanged = (messageText) => ({
    type: actionTypes.INPUT_CHANGED,
    messageText: messageText,
});

export const fetchUser = (userId) => {
    return { type: actionTypes.FETCH_USER, userId: userId };
};

export const fetchUserSuccess = (userData) => {
    return {
        type: actionTypes.FETCH_USER_SUCCESS,
        userData: userData,
    };
};

export const fetchUserFailed = (errorMessage) => ({
    type: actionTypes.FETCH_USER_FAILED,
    errorMessage: errorMessage,
});

export const userEditorInputChanged = (newInput, field) => ({
    type: actionTypes.USER_EDITOR_INPUT_CHANGED,
    newInput: newInput,
    field: field,
});

export const saveUserEditorData = (id, userData) => ({
    type: actionTypes.SAVE_USER_EDITOR_DATA,
    id: id,
    userData: userData,
});

export const saveUserEditorDataSuccess = (userData) => ({
    type: actionTypes.SAVE_USER_EDITOR_DATA_SUCCESS,
    userData: userData,
});

export const saveUserEditorDataFailed = (userData) => ({
    type: actionTypes.SAVE_USER_EDITOR_DATA_FAILED,
    userData: userData,
});

export const fetchUsersList = () => ({
    type: actionTypes.FETCH_USERS_LIST,
});

export const fetchUsersListSuccess = (users) => ({
    type: actionTypes.FETCH_USERS_LIST_SUCCESS,
    users: users,
});

export const fetchUsersListFailed = (errorMessage) => ({
    type: actionTypes.FETCH_USERS_LIST_FAILED,
    errorMessage: errorMessage,
});

export const deleteUser = (id) => ({
    type: actionTypes.DELETE_USER,
    userId: id,
});

export const deleteUserSuccess = (users) => ({
    type: actionTypes.DELETE_USER_SUCCESS,
    users: users,
});

export const deleteUserFailed = (errorMessage) => ({
    type: actionTypes.DELETE_USER_FAILED,
    errorMessage: errorMessage,
});

export const loginUser = (email, password) => ({
    type: actionTypes.LOGIN_USER,
    email: email,
    password: password,
});

export const loginUserSuccess = (userData) => ({
    type: actionTypes.LOGIN_USER_SUCCESS,
    userData: userData,
});

export const loginUserFailed = (errorMessage) => ({
    type: actionTypes.LOGIN_USER_FAILED,
    errorMessage: errorMessage,
});

export const resetProps = () => ({
    type: actionTypes.RESET_EDITOR_PROPS,
});

export const fetchMessages = () => ({
    type: actionTypes.FETCH_MESSAGES,
});

export const fetchMessagesSuccess = (messages) => ({
    type: actionTypes.FETCH_MESSAGES_SUCCESS,
    messages: messages,
});

export const fetchMessagesFailed = (errorMessage) => ({
    type: actionTypes.FETCH_MESSAGES_FAILED,
    errorMessage: errorMessage,
});
