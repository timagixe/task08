import * as actionTypes from '../actions/actionTypes';

const initialState = {
    users: [],
    error: {
        state: false,
        message: '',
    },
};

const fetchUsersListSuccess = (state, action) => {
    return {
        ...state,
        users: action.users,
    };
};

const fetchUsersListFailed = (state, action) => {
    return {
        ...state,
        error: {
            state: true,
            message: action.errorMessage.response.data,
        },
    };
};

const deleteUserSuccess = (state, action) => {
    return {
        ...state,
        users: action.users,
    };
};

const deleteUserFailed = (state, action) => {
    return {
        ...state,
        error: {
            state: true,
            message: action.errorMessage.response.data,
        },
    };
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_USERS_LIST_SUCCESS:
            return fetchUsersListSuccess(state, action);
        case actionTypes.FETCH_USERS_LIST_FAILED:
            return fetchUsersListFailed(state, action);
        case actionTypes.DELETE_USER_SUCCESS:
            return deleteUserSuccess(state, action);
        case actionTypes.DELETE_USER_FAILED:
            return deleteUserFailed(state, action);
        default:
            return state;
    }
};

export default usersReducer;
