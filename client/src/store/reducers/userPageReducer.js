import * as actionTypes from '../actions/actionTypes';

const initialState = {
    firstName: '',
    lastName: '',
    userId: '',
    email: '',
    password: '',
    error: {
        state: false,
        message: '',
    },
};

const fetchUserSuccess = (state, action) => {
    return {
        ...state,
        ...action.userData.data,
    };
};

const fetchUserFailed = (state, action) => {
    return {
        ...state,
        error: {
            state: !state.error.state,
            message: action.errorMessage.response.data,
        },
    };
};

const userEditorInputChanged = (state, action) => ({
    ...state,
    [action.field]: action.newInput,
});

const saveUserEditorDataSuccess = (state, action) => {
    return {
        ...state,
        ...action.userData,
    };
};

const saveUserEditorDataFailed = (state, action) => {
    return {
        ...state,
        error: {
            state: true,
            message: action.errorMessage.response.data,
        },
    };
};

const userPageReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_USER_SUCCESS:
            return fetchUserSuccess(state, action);
        case actionTypes.FETCH_USER_FAILED:
            return fetchUserFailed(state, action);
        case actionTypes.USER_EDITOR_INPUT_CHANGED:
            return userEditorInputChanged(state, action);
        case actionTypes.SAVE_USER_EDITOR_DATA_SUCCESS:
            return saveUserEditorDataSuccess(state, action);
        case actionTypes.SAVE_USER_EDITOR_DATA_FAILED:
            return saveUserEditorDataFailed(state, action);
        case actionTypes.RESET_EDITOR_PROPS:
            return {
                ...state,
                firstName: '',
                lastName: '',
                userId: '',
                email: '',
                password: '',
            };
        default:
            return state;
    }
};

export default userPageReducer;
