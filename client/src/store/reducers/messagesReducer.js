import * as actionTypes from '../actions/actionTypes';

const initialState = {
    messages: [],
    error: '',
};

const fetchMessages = (state, action) => {
    return { ...state, messages: action.messages };
};

const fetchMessagesFailed = (state, action) => {
    return {
        ...state,
        error: action.errorMessage,
    };
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_MESSAGES_SUCCESS:
            return fetchMessages(state, action);
        case actionTypes.FETCH_MESSAGES_FAILED:
            return fetchMessagesFailed(state, action);
        default:
            return state;
    }
};

export default messagesReducer;
