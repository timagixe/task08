import * as actionTypes from '../actions/actionTypes';

const initialState = {
    showModal: false,
    messageText: null,
    messageId: null,
};

const changeModalStatus = (state, action) =>
    action.messageText && action.messageId
        ? {
              ...state,
              showModal: !state.showModal,
              messageText: action.messageText,
              messageId: action.messageId,
          }
        : {
              ...state,
              showModal: !state.showModal,
              messageText: '',
              messageId: '',
          };

const changeModalText = (state, action) => ({
    ...state,
    messageText: action.newText,
});

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_MODAL_STATUS:
            return changeModalStatus(state, action);
        case actionTypes.CHANGE_MODAL_TEXT:
            return changeModalText(state, action);
        default:
            return state;
    }
};

export default modalReducer;
