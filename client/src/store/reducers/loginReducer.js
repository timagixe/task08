import * as actionTypes from '../actions/actionTypes';

const initialState = {
    firstName: '',
    lastName: '',
    userId: '',
    email: '',
    password: '',
};

const loginUserSuccess = (state, action) => {
    return {
        ...initialState,
        ...action.userData,
    };
};

const loginUserFailed = (state, action) => ({
    ...state,
    error: {
        state: true,
        message: action.errorMessage,
    },
});

const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOGIN_USER_SUCCESS:
            return loginUserSuccess(state, action);
        case actionTypes.LOGIN_USER_FAILED:
            return loginUserFailed(state, action);
        default:
            return state;
    }
};

export default loginReducer;
