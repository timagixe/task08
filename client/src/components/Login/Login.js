import React, { useState, useEffect } from 'react';
import * as actions from '../../store/actions/actionCreators';
import { connect } from 'react-redux';

const Login = (props) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    if (sessionStorage.getItem('loggedIn')) {
        props.history.push('/chat');
    }

    const handleLogin = (email, password) => {
        if (password === '' || email === '') {
            alert('One of the fields is empty');
        } else {
            props.onLogin(email, password);
        }
    };

    useEffect(() => {
        if (props.userDetails.email) {
            sessionStorage.setItem('email', props.userDetails.email);
        }
        if (!!sessionStorage.getItem('email')) {
            if (sessionStorage.getItem('email') === 'admin') {
                props.history.push('/users');
            } else {
                props.history.push('/chat');
            }
        }
    }, [props.history, props.userDetails.email]);

    return (
        <>
            <input
                onKeyDown={(event) =>
                    event.key === 'Enter' ? handleLogin(email, password) : null
                }
                id='login'
                placeholder='Enter your login'
                onChange={(event) => {
                    setEmail(event.target.value);
                }}
            ></input>

            <input
                onKeyDown={(event) =>
                    event.key === 'Enter' ? handleLogin(email, password) : null
                }
                id='password'
                placeholder='Enter your password'
                onChange={(event) => {
                    setPassword(event.target.value);
                }}
            ></input>

            <button
                onClick={() => {
                    handleLogin(email, password);
                }}
            >
                Login
            </button>
        </>
    );
};

const mapStateToProps = (state) => ({
    userDetails: state.loginReducer,
});

const mapDispatchToProps = (dispatch) => ({
    onLogin: (email, password) => dispatch(actions.loginUser(email, password)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
