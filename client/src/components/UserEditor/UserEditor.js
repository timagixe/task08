import React from 'react';

import * as actions from '../../store/actions/actionCreators';
import { connect } from 'react-redux';

class UserEditor extends React.Component {
    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.onFetchUser(this.props.match.params.id);
        }

        if (
            !(
                !!sessionStorage.getItem('email') &&
                sessionStorage.getItem('email') === 'admin'
            )
        ) {
            this.props.history.push('/chat');
        }
    }

    getUserDataHandler() {
        return {
            firstName: this.props.firstName,
            lastName: this.props.lastName,
            userId: String(this.props.userId),
            email: this.props.email,
            password: this.props.password,
        };
    }

    render() {
        return (
            <div>
                <input
                    placeholder='First Name'
                    value={this.props.firstName}
                    onChange={(event) =>
                        this.props.onInputChange(
                            event.target.value,
                            'firstName'
                        )
                    }
                ></input>

                <input
                    placeholder='Last Name'
                    value={this.props.lastName}
                    onChange={(event) =>
                        this.props.onInputChange(event.target.value, 'lastName')
                    }
                ></input>

                <input
                    placeholder='Email'
                    value={this.props.email}
                    onChange={(event) =>
                        this.props.onInputChange(event.target.value, 'email')
                    }
                ></input>

                <input
                    placeholder='Password'
                    value={this.props.password}
                    onChange={(event) =>
                        this.props.onInputChange(event.target.value, 'password')
                    }
                ></input>

                <button
                    onClick={() => {
                        this.props.history.push('/users');
                        this.props.resetProps();
                    }}
                >
                    Go back
                </button>
                <button
                    onClick={() => {
                        const userData = this.getUserDataHandler();

                        this.props.match.params.id
                            ? this.props.onSaveClick(
                                  this.props.match.params.id,
                                  userData
                              )
                            : this.props.onSaveClick(null, userData);

                        this.props.history.push('/users');
                        this.props.resetProps();
                    }}
                >
                    {this.props.match.params.id ? 'Save' : 'Add'}
                </button>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    firstName: state.userPageReducer.firstName,
    lastName: state.userPageReducer.lastName,
    userId: state.userPageReducer.userId,
    email: state.userPageReducer.email,
    password: state.userPageReducer.password,
});

const mapDispatchToProps = (dispatch) => ({
    onFetchUser: (id) => dispatch(actions.fetchUser(id)),
    onInputChange: (text, field) =>
        dispatch(actions.userEditorInputChanged(text, field)),
    onSaveClick: (id, userData) =>
        dispatch(actions.saveUserEditorData(id, userData)),
    resetProps: () => dispatch(actions.resetProps()),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserEditor);
