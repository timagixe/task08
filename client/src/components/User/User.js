import React from 'react';
import * as actions from '../../store/actions/actionCreators';
import { connect } from 'react-redux';
import './User.css';

const User = (props) => {
    const { userId, firstName, lastName, email } = props;

    const onEditHandler = (id) => {
        props.history.push(`/edit/${id}`);
    };

    const onDeleteHandler = (id) => {
        props.onDeleteUser(id);
        window.location.reload(true);
    };

    return (
        <ul>
            <li>
                <span id='firstName'>{firstName}</span>{' '}
                <span id='lastName'>{lastName}</span>
            </li>
            <li>
                <span id='email'>{email}</span>{' '}
            </li>
            <li>
                <div id='editButton' onClick={onEditHandler.bind(null, userId)}>
                    Edit
                </div>{' '}
            </li>
            <li>
                <div
                    id='deleteButton'
                    onClick={() => onDeleteHandler(props.userId)}
                >
                    Delete
                </div>{' '}
            </li>
        </ul>
    );
};

const mapDispatchToProps = (dispatch) => ({
    onDeleteUser: (id) => dispatch(actions.deleteUser(id)),
});

export default connect(null, mapDispatchToProps)(User);
