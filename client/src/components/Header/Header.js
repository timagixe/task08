import React from 'react';

import './Header.css';

const header = (props) => {
    return (
        <div className='top_menu'>
            <div className='title'>Binary Chat</div>
            <div className='title'>
                {props.chatParticipantsCount} participants
            </div>
            <div className='title'>{props.chatMessagesCount} messages</div>
            <div className='title' id='lastMessageAt'>
                Last message at {props.lastMessageTime}
            </div>
        </div>
    );
};

export default header;
